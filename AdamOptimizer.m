classdef AdamOptimizer
    % ADAMOPTIMIZER - Adam optimizer.
    % See for more information:
    % [1] Kingma, D., & Ba, J. (2014). Adam: A method for stochastic optimization. 
    % arXiv preprint arXiv:1412.6980.
    methods(Static)
        
        function [net, fval, gval] = optimize(net, X, Y, options)
            
            curr_idx = 1;
            w = net.getWeights();
            fval = zeros(options.epochs, 1);
            gval = zeros(options.epochs, 1);
            
            mt = zeros(length(w), 1); % Initialize first-order moments
            vt = zeros(length(w), 1); % Initialize second-order moments
            
            for n = 1:options.epochs
                
                if(curr_idx+options.batch_size > size(X, 1))
                    idx = curr_idx:size(X, 1);
                    curr_idx = 1;
                    if(options.batch_size < Inf)
                        % Randomly shuffle dataset
                        shuff = randperm(size(X, 1));
                        X = X(shuff, :);
                        Y = Y(shuff, :);
                    end
                else
                    idx = curr_idx:curr_idx + options.batch_size;
                    curr_idx = curr_idx + options.batch_size;
                end
                
                X_batch = X(idx, :);
                Y_batch = Y(idx, :);
                
                [pre, post, opt] = net.forwardPass(X_batch);
                [g_l, g_r] = net.backwardPass(X_batch, Y_batch, pre, post, opt, options);
                
                grads = g_l + g_r;
                
                mt = options.beta1*mt + (1-options.beta1)*grads;
                vt = options.beta2*vt + (1-options.beta2)*grads.^2;
                mt_hat = mt/(1-options.beta1^n);
                vt_hat = vt/(1-options.beta2^n);
                w = w - options.eta*mt_hat./(sqrt(vt_hat + options.eps_adam));
                
                fval(n) = net.computeObjectiveFunction(X_batch, Y_batch, pre, post, w, opt, options);
                gval(n) = norm(grads);
                
                net = net.reshapeWeights(w);
                
            end
        end
        
    end
    
end
    
