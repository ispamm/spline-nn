classdef StandardNNTest < matlab.unittest.TestCase

    
    methods (Test)

        function testForwardPass(testCase)
            
            n = StandardNN('', 2);
            
            X = [0.1 0.2; 0.3 0.4;];
            Y = [0.4 0.5; 0.1 0.6];
            n = n.init(X, Y);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 2)*2 - 1;
            n.W = {W_in_h, W_h_out};
            
            % Forward pass for input 1 --> hidden node 1
            h1 = X(1,1)*W_in_h(1,1) + X(1,2)*W_in_h(2,1) + W_in_h(3,1);
            x1 = tanh(h1);
            
            % Forward pass for input 1 --> hidden node 2
            h2 = X(1,1)*W_in_h(1,2) + X(1,2)*W_in_h(2,2) + W_in_h(3,2);
            x2 = tanh(h2);
            
            % Forward pass hidden nodes --> output node
            o1 = x1*W_h_out(1,1) + x2*W_h_out(2,1) + W_h_out(3,1);
            o2 = x1*W_h_out(1,2) + x2*W_h_out(2,2) + W_h_out(3,2);
            
            % Make full forward pass in network
            [pre, post] = n.forwardPass(X);
            
            % Check validity of results
            testCase.assertLessThan(abs(pre{1}(1,:) - [h1 h2]), 10^-10);
            testCase.assertLessThan(abs(post{1}(1,:) - [x1 x2]), 10^-10);
            testCase.assertLessThan(abs(pre{2}(1,:) - [o1 o2]), 10^-10);
            testCase.assertLessThan(abs(post{2}(1,:) - tanh([o1, o2])), 10^-10);
            
            
        end
        
        function testForwardPass_2layers(testCase)
            
            n = StandardNN('', [2, 2]);
            
            X = [0.1 0.2; 0.3 0.4;];
            Y = [0.4 0.5; 0.1 0.6];
            n = n.init(X, Y);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 2)*2 - 1;
            n.W = {W_in_h, W_h_h, W_h_out};
            
            % Forward pass for input 1 --> hidden node 1
            h1 = X(1,1)*W_in_h(1,1) + X(1,2)*W_in_h(2,1) + W_in_h(3,1);
            x1 = tanh(h1);
            
            % Forward pass for input 1 --> hidden node 2
            h2 = X(1,1)*W_in_h(1,2) + X(1,2)*W_in_h(2,2) + W_in_h(3,2);
            x2 = tanh(h2);
            
            % Forward pass hidden nodes --> hidden nodes
            s1 = x1*W_h_h(1,1) + x2*W_h_h(2,1) + W_h_h(3,1);
            s2 = x1*W_h_h(1,2) + x2*W_h_h(2,2) + W_h_h(3,2);
            p1 = tanh(s1);
            p2 = tanh(s2);
            
            % Forward pass hidden nodes --> output node
            o1 = p1*W_h_out(1,1) + p2*W_h_out(2,1) + W_h_out(3,1);
            o2 = p1*W_h_out(1,2) + p2*W_h_out(2,2) + W_h_out(3,2);
            
            % Make full forward pass in network
            [pre, post] = n.forwardPass(X);
            
            % Check validity of results
            testCase.assertLessThan(abs(pre{1}(1,:) - [h1 h2]), 10^-10);
            testCase.assertLessThan(abs(post{1}(1,:) - [x1 x2]), 10^-10);
            testCase.assertLessThan(abs(pre{2}(1,:) - [s1 s2]), 10^-10);
            testCase.assertLessThan(abs(post{2}(1,:) - [p1, p2]), 10^-10);
            testCase.assertLessThan(abs(pre{3}(1,:) - [o1 o2]), 10^-10);
            testCase.assertLessThan(abs(post{3}(1,:) - tanh([o1, o2])), 10^-10);

        end
        
        function testBackwardPass_1dinput(testCase)
            
            n = StandardNN('', 2);
            X = [0.1; 0.3];
            Y = [0.4; 0.2];
            
            n = n.init(X, Y);
            
            % Set manually the weights
            W_in_h = rand(2, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n = n.reshapeWeights([W_in_h(:); W_h_out(:)]);
            
            % Define cost function
            sW_in_h = sym('sW_in_h', [2, 2], 'real');
            sW_h_out = sym('sW_h_out', [3, 1], 'real');
            f_net = @(x) tanh(sW_h_out'*([tanh(sW_in_h'*[x; 1]); 1]));
            squaredError = ((Y(1)' - f_net(X(1)'))^2 + (Y(2)' - f_net(X(2)'))^2)/2;
            regularizer = norm([sW_in_h(:); sW_h_out(:)])^2;
            
            % Compute symbolic gradients
            sGrads_squaredError = gradient(squaredError, [sW_in_h(:); sW_h_out(:)]);
            sGrads_squaredError = double(subs(sGrads_squaredError, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            sGrads_regularizer = gradient(regularizer, [sW_in_h(:); sW_h_out(:)]);
            sGrads_regularizer = double(subs(sGrads_regularizer, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            
            % Make forward pass in network
            [pre, post] = n.forwardPass(X);

            % Make backward pass in network
            [grads_l, grads_r] = n.backwardPass(X, Y, pre, post, [], struct('lambda', 1));
            
            % Check validity of results
            testCase.assertLessThan(abs(sGrads_squaredError - grads_l), 10^-10);
            testCase.assertLessThan(abs(sGrads_regularizer - grads_r), 10^-10);
            
        end
        
        function testBackwardPass_2dinput(testCase)
            
            n = StandardNN('', 2);
            X = [0.1 0.2; 0.3 0.4];
            Y = [-0.3; 0.2];
            
            n = n.init(X, Y);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n = n.reshapeWeights([W_in_h(:); W_h_out(:)]);

            % Define cost function
            sW_in_h = sym('sW_in_h', [3, 2], 'real');
            sW_h_out = sym('sW_h_out', [3, 1], 'real');
            f_net = @(x) tanh(sW_h_out'*([tanh(sW_in_h'*[x; 1]); 1]));
            squaredError = (sum((Y(1,:)' - f_net(X(1,:)')).^2) + sum((Y(2,:)' - f_net(X(2,:)')).^2))/2;
            regularizer = norm([sW_in_h(:); sW_h_out(:)])^2;
            
            % Compute symbolic gradients
            sGrads_squaredError = gradient(squaredError, [sW_in_h(:); sW_h_out(:)]);
            sGrads_squaredError = double(subs(sGrads_squaredError, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            sGrads_regularizer = gradient(regularizer, [sW_in_h(:); sW_h_out(:)]);
            sGrads_regularizer = double(subs(sGrads_regularizer, [sW_in_h(:); sW_h_out(:)], [W_in_h(:); W_h_out(:)]));
            
            % Make forward pass in network
            [pre, post] = n.forwardPass(X);

            % Make backward pass in network
            [grads_l, grads_r] = n.backwardPass(X, Y, pre, post, [], struct('lambda', 1));
            
            % Check validity of results
            testCase.assertLessThan(abs(sGrads_squaredError - grads_l), 10^-10);
            testCase.assertLessThan(abs(sGrads_regularizer - grads_r), 10^-10);
            
        end
        
        function testBackwardPass_2hiddenlayers(testCase)
            
            n = StandardNN('', [2, 2]);
            X = [0.1 0.2; 0.3 0.4];
            Y = [-0.3; 0.2];
            
            n = n.init(X, Y);
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 1)*2 - 1;
            n = n.reshapeWeights([W_in_h(:); W_h_h(:); W_h_out(:)]);

            % Define cost function
            sW_in_h = sym('sW_in_h', [3, 2], 'real');
            sW_h_h = sym('sW_h_h', [3, 2], 'real');
            sW_h_out = sym('sW_h_out', [3, 1], 'real');
            f_net = @(x) tanh(sW_h_out'*[tanh(sW_h_h'*[tanh(sW_in_h'*[x; 1]); 1]); 1]);
            squaredError = ((Y(1,:)' - f_net(X(1,:)')).^2 + (Y(2,:)' - f_net(X(2,:)')).^2)/2;
            regularizer = norm([sW_in_h(:); sW_h_h(:); sW_h_out(:)])^2;
            
            % Compute symbolic gradients
            sGrads_squaredError = gradient(squaredError, [sW_in_h(:); sW_h_h(:); sW_h_out(:)]);
            sGrads_squaredError = double(subs(sGrads_squaredError, [sW_in_h(:); sW_h_h(:); sW_h_out(:)], [W_in_h(:); W_h_h(:); W_h_out(:)]));
            sGrads_regularizer = gradient(regularizer, [sW_in_h(:); sW_h_h(:); sW_h_out(:)]);
            sGrads_regularizer = double(subs(sGrads_regularizer, [sW_in_h(:); sW_h_h(:); sW_h_out(:)], [W_in_h(:); W_h_h(:); W_h_out(:)]));
            
            % Make forward pass in network
            [pre, post] = n.forwardPass(X);

            % Make backward pass in network
            [grads_l, grads_r] = n.backwardPass(X, Y, pre, post, [], struct('lambda', 1));
            
            % Check validity of results
            testCase.assertLessThan(abs(sGrads_squaredError - grads_l), 10^-10);
            testCase.assertLessThan(abs(sGrads_regularizer - grads_r), 10^-10);
            
        end

    end  
    
end

