
% RUN_TEST_SUITE Run all the test suite.
%   Unitary tests are contained in the 'tests' folder. This code requires
%   MATLAB v8.1 or higher to run.

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

% Add the path and check for the correct version
addpath(pwd, genpath('../utils/'), genpath('./'));
if verLessThan('matlab', '8.1')
    error('This Matlab version does not support unitary testing. To run the test suite, please upgrade to version 8.1 or higher.');
end

import matlab.unittest.TestSuite;
clc;

p = 0; f = 0;

% Start timer
t = clock;
% Open the tests in the folder
fold = './';
suiteFolder = [TestSuite.fromClass(?StandardNNTest),...
    TestSuite.fromClass(?SplineNNTest)];
result = run(suiteFolder);

fprintf('\n');
cprintf('*text', 'Tests for folder %s: \n', fold);

% Iteratively run each test class
for i=1:length(result)
    
    if(result(i).Passed)
        fprintf('\n');
        cprintf('comment', '\t Test %s: PASSED', result(i).Name);
    else
        fprintf('\n');
        cprintf('err', '\t Test %s: NOT PASSED', result(i).Name);
    end
end

fprintf('\n\n\n');

% Total counts of passed and/or failed tests
for i=1:length(result)
    p = p + result(i).Passed;
    f = f + result(i).Failed;
end

% Print summary
fprintf('Total passed tests: %i.\nTotal failed tests: %i.\n', p, f);
fprintf('Total testing time: %.2f secs\n', etime(clock, t));