classdef SplineNNTest < matlab.unittest.TestCase

    
    methods (Test)

        function testForwardPass(testCase)
            
            n = SplineNN('', 2, 'spline_init_fcn', @(x)x);
            
            X = [0.1 0.2; 0.3 0.4;];
            Y = [0.6 0.1; -0.2 0.3];
            n = n.init(X, Y, struct());
            
            % Set manually the weights
            W_in_h = rand(3, 2)*2 - 1;
            W_h_out = rand(3, 2)*2 - 1;
            n.W = {W_in_h, W_h_out};
            q_h = rand(23, 2);
            n.q{1} = q_h;
            q_out = rand(23, 2);
            n.q{2} = q_out;
            
            % Forward pass for input 1 --> hidden node 1
            h1 = X(1,1)*W_in_h(1,1) + X(1,2)*W_in_h(2,1) + W_in_h(3,1);
            idx = floor(h1/n.sampling_step) + 11;
            u = h1/n.sampling_step - floor(h1/n.sampling_step);
            uvec = [u^3, u^2, u, 1];
            % Bound checking
            idx(idx<1) = 1;
            % The index must fit
            idx(idx>(n.N_control_points-3))= n.N_control_points - 3;
            x1 = uvec*n.B*q_h(idx:idx+3,1);
            
            
            % Forward pass for input 1 --> hidden node 2
            h2 = X(1,1)*W_in_h(1,2) + X(1,2)*W_in_h(2,2) + W_in_h(3,2);
            idx = floor(h2/n.sampling_step) + 11;
            u = h2/n.sampling_step - floor(h2/n.sampling_step);
            uvec = [u^3, u^2, u, 1];
            % Bound checking
            idx(idx<1) = 1;
            % The index must fit
            idx(idx>(n.N_control_points-3))= n.N_control_points - 3;
            x2 = uvec*n.B*q_h(idx:idx+3,2);
            
            % Forward pass hidden nodes --> output node
            o1 = x1*W_h_out(1,1) + x2*W_h_out(2,1) + W_h_out(3,1);
            o2 = x1*W_h_out(1,2) + x2*W_h_out(2,2) + W_h_out(3,2);
            
            Sidx = o1/n.sampling_step + 11;
            idx = floor(Sidx);
            u = Sidx - idx;
            uvec = [u^3, u^2, u, 1];
            % Bound checking
            idx(idx<1) = 1;
            % The index must fit
            idx(idx>(n.N_control_points-3))= n.N_control_points - 3;
            y1 = uvec*n.B*n.q{2}(idx:idx+3,1);
            
            Sidx = o2/n.sampling_step + 11;
            idx = floor(Sidx);
            u = Sidx - idx;
            uvec = [u^3, u^2, u, 1];
            % Bound checking
            idx(idx<1) = 1;
            % The index must fit
            idx(idx>(n.N_control_points-3))= n.N_control_points - 3;
            y2 = uvec*n.B*n.q{2}(idx:idx+3,2);
            
            % Make full forward pass in network
            [pre, post] = n.forwardPass(X);
            
            % Check validity of results
            testCase.assertLessThan(abs(pre{1}(1,:) - [h1 h2]), 10^-10);
            testCase.assertLessThan(abs(post{1}(1,:) - [x1 x2]), 10^-10);
            testCase.assertLessThan(abs(pre{2}(1,:) - [o1 o2]), 10^-10);
            testCase.assertLessThan(abs(post{2}(1,:) - [y1, y2]), 10^-10);
            
            
        end
        
        function test_spline_derivatives(testCase)
            
            init_fcns = {@tanh, @(s)s.^2, @(s)s, @(s) exp(-s)};
            d_init_fcns = {@(s)(1-tanh(s).^2), @(s) 2*s, @(s) s.^0, @(s) -exp(-s)};
            dd_init_fcns = {@(s) -2*tanh(s).*(1-tanh(s).^2), @(s) 2, @(s) 0, @(s) exp(-s)};    
            
            for f = 1:length(init_fcns)

                n = SplineNN('', 1, 'spline_init_fcn', init_fcns{f}, 'sampling_step', 0.00001, 'spline_init_noise', 0);
                n = n.init([0.1], [0.2], struct());

                s_to_test = -1:0.01:+1;
                [u, uIndex] = n.getSplineIndexes(s_to_test');
                [ds, dds] = n.getSplineDerivatives(s_to_test', u, uIndex, 1);
                ds_real = d_init_fcns{f}(s_to_test)';
                dds_real = dd_init_fcns{f}(s_to_test)';
                
                testCase.assertLessThan(abs(ds_real-ds), 10^-4);
                testCase.assertLessThan(abs(dds_real-dds), 10^-4);
            end
            
        end
       
        function testBackwardPass_2dinput_symbolic(testCase)
            
            n = SplineNN('', 2, 'spline_init_fcn', @(x) x, 'sampling_step', 10^-6);
            X = [0.1 0.2; 0.3 0.4];
            Y = [0.6 0.1; -0.2 0.3];
            
            n = n.init(X, Y, struct());
            
            % Set manually the weights
            W_in_h = rand(3, 2)*0.5 - 0.25;
            W_h_out = rand(3, 2)*0.5 - 0.25;
            n.W = {W_in_h, W_h_out};
            
            sigmoid = @(s) 1./(1+exp(-s));
            
            % Set manually four different nonlinearities
            q_h1 = tanh(n.control_points);
            n.q{1}(:, 1) = q_h1;
            q_h2 = n.control_points;
            n.q{1}(:, 2) = q_h2;
            q_out1 = sigmoid(n.control_points);
            n.q{2}(:, 1) = q_out1;
            q_out2 = n.control_points.^2;
            n.q{2}(:, 2) = q_out2;
            
            % Define cost function
            sW_in_h = sym('sW_in_h', [3, 2], 'real');
            sW_h_out = sym('sW_h_out', [3, 2], 'real');
            f_net = @(x) [sigmoid(sW_h_out(:, 1)'*[tanh(sW_in_h(:, 1)'*[x; 1]); (sW_in_h(:, 2)'*[x; 1]); 1]); ...
                (sW_h_out(:, 2)'*[tanh(sW_in_h(:, 1)'*[x; 1]); (sW_in_h(:, 2)'*[x; 1]); 1])^2];
            squaredError = (sum((Y(1,:)' - f_net(X(1,:)')).^2) + sum((Y(2,:)' - f_net(X(2,:)')).^2))/2;
            
            % Compute symbolic gradients
            sGrads_squaredError = gradient(squaredError, [sW_in_h(:); sW_h_out(:);]);
            sGrads_squaredError = double(subs(sGrads_squaredError, [sW_in_h(:); sW_h_out(:);], [W_in_h(:); W_h_out(:);]));
            
            % Make forward pass in network
            [pre, post, opt] = n.forwardPass(X);

            % Make backward pass in network
            grads_l = n.backwardPass(X, Y, pre, post, opt);
            
            % Check validity of results
            testCase.assertLessThan(abs(sGrads_squaredError - grads_l(1:12)), 10^-3);
            
        end
        
        function testBackwardPass_2dinput_manual(testCase)
            
            n = SplineNN('', 2);
            X = [0.1 -0.2;];
            Y = [0.6 0.1;];
            
            n = n.init(X, Y, struct());
            
            % Set random weights
            n.W = {randn(3, 2)*0.2, randn(3, 2)*0.2};
            
            % Set random spline control points
            n.q{1} = rand(n.N_control_points, 2);
            n.q{2} = rand(n.N_control_points, 2);
            n.q0 = [rand(n.N_control_points*2, 1); rand(n.N_control_points*2, 1)];
            
            % Set vectorized vector
            n.theta = [n.vectorizeCellArray(n.W); n.vectorizeCellArray(n.q)];
            
            % Make forward pass in network
            [pre, post, opt] = n.forwardPass(X);

            % Make backward pass in network
            [grads_l, grads_r] = n.backwardPass(X, Y, pre, post, opt, struct('lambda', 0.5, 'lambda_q0', 0.8));
            
            % Make manual backward pass
            res = testCase.makeManualBackwardPass(n, X, Y, pre, post);
            
            % Extract subvectors from computer gradients' vector
            wh_est = grads_l(1:numel(n.W{1}));
            z = numel(n.W{2}) + 1;
            wo_est = grads_l(z:z+numel(n.W{2})-1);
            z = z + numel(n.W{2});
            qh_est = grads_l(z:z+numel(n.q{1})-1);
            z = z + numel(n.q{1});
            qo_est = grads_l(z:z+numel(n.q{2})-1);
            
            % Compute regularization term
            g_reg_est = [n.vectorizeCellArray(n.W); 1.6*(n.vectorizeCellArray(n.q) - n.q0)];
            
            % Check everything
            testCase.assertLessThan(abs(qo_est - [res.qo_1; res.qo_2]), 10^-6);
            testCase.assertLessThan(abs(wo_est- [res.wo_11; res.wo_21; res.wo_1; res.wo_12; res.wo_22; res.wo_2]), 10^-6);
            testCase.assertLessThan(abs(qh_est - [res.qh_1; res.qh_2]), 10^-6);
            testCase.assertLessThan(abs(wh_est - [res.wh_11; res.wh_21; res.wh_1; res.wh_12; res.wh_22; res.wh_2]), 10^-6);
            testCase.assertLessThan(abs(g_reg_est - grads_r), 10^-6);
        end
        
        function testBackwardPass_2layers_manual(testCase)
            
            n = SplineNN('', [2, 2]);
            X = [0.1 -0.2;];
            Y = 0.6;
            
            n = n.init(X, Y, struct());
            
            % Set random weights
            n.W = {randn(3, 2)*0.2, randn(3, 2)*0.2, randn(3, 1)*0.2};
            
            % Set random spline control points
            n.q = {rand(n.N_control_points, 2), rand(n.N_control_points, 2), rand(n.N_control_points, 1)};
            
            % Make forward pass in network
            [pre, post, opt] = n.forwardPass(X);

            % Make backward pass in network
            grads_l = n.backwardPass(X, Y, pre, post, opt);
            
            % Compute errors
            e = (Y - post{3});
            
            % Prestore all the indexes
            [u1, u1Index] = getSplineIndexes(n, pre{1});
            [u2, u2Index] = getSplineIndexes(n, pre{2});
            [u3, u3Index] = getSplineIndexes(n, pre{3});
            
            % Precompute all spline derivatives
            [dphi1] = n.getSplineDerivatives(pre{1}, u1', u1Index', 1);
            [dphi2] = n.getSplineDerivatives(pre{2}, u2', u2Index', 2);
            [dphi3] = n.getSplineDerivatives(pre{3}, u3', u3Index', 3);
            
            % Gradients for q{3}
            q3 = -2*e*n.B'*[u3(1)^3; u3(1)^2; u3(1); 1];
            res.q3 = [zeros(u3Index(1)-1, 1); q3; zeros(n.N_control_points - u3Index(1)-3, 1)];
            
            % Gradients for w{3}
            res.w3_0 = -2*e*dphi3;
            res.w3_1 = -2*e*dphi3*post{2}(1);
            res.w3_2 = -2*e*dphi3*post{2}(2);
            
            % Gradients for q{2}
            res.q2_1 = -2*e*dphi3*n.W{3}(1)*n.B'*[u2(1)^3; u2(1)^2; u2(1); 1];
            res.q2_1 = [zeros(u2Index(1)-1, 1); res.q2_1; zeros(n.N_control_points - u2Index(1)-3, 1)];
            res.q2_2 = -2*e*dphi3*n.W{3}(2)*n.B'*[u2(2)^3; u2(2)^2; u2(2); 1];
            res.q2_2 = [zeros(u2Index(2)-1, 1); res.q2_2; zeros(n.N_control_points - u2Index(2)-3, 1)];
            
            % Gradients for w{2}
            res.w2_10 = -2*e*dphi3*n.W{3}(1)*dphi2(1);
            res.w2_20 = -2*e*dphi3*n.W{3}(2)*dphi2(2);
            res.w2_11 = -2*e*dphi3*n.W{3}(1)*dphi2(1)*post{1}(1);
            res.w2_12 = -2*e*dphi3*n.W{3}(2)*dphi2(2)*post{1}(1);
            res.w2_21 = -2*e*dphi3*n.W{3}(1)*dphi2(1)*post{1}(2);
            res.w2_22 = -2*e*dphi3*n.W{3}(2)*dphi2(2)*post{1}(2);
            
            % Intermediate values
            delta1 = -2*e*dphi3*n.W{3}(1)*dphi2(1)*n.W{2}(1, 1) - ...
                2*e*dphi3*n.W{3}(2)*dphi2(2)*n.W{2}(1, 2);
            delta2 = -2*e*dphi3*n.W{3}(1)*dphi2(1)*n.W{2}(2, 1) - ...
                2*e*dphi3*n.W{3}(2)*dphi2(2)*n.W{2}(2, 2);
            
            % Gradients for q{1}
            res.q1_1 = delta1*n.B'*[u1(1)^3; u1(1)^2; u1(1); 1];
            res.q1_1 = [zeros(u1Index(1)-1, 1); res.q1_1; zeros(n.N_control_points - u1Index(1)-3, 1)];
            res.q1_2 = delta2*n.B'*[u1(2)^3; u1(2)^2; u1(2); 1];
            res.q1_2 = [zeros(u1Index(2)-1, 1); res.q1_2; zeros(n.N_control_points - u1Index(2)-3, 1)];
            
            % Gradients for w{1}
            res.w1_10 = delta1*dphi1(1);
            res.w1_20 = delta2*dphi1(2);
            res.w1_11 = delta1*dphi1(1)*X(1);
            res.w1_12 = delta2*dphi1(2)*X(1);
            res.w1_21 = delta1*dphi1(1)*X(2);
            res.w1_22 = delta2*dphi1(2)*X(2);
            
            % Extract subvectors from computed gradients' vector
            w1_est = grads_l(1:numel(n.W{1}));
            z = numel(n.W{1}) + 1;
            w2_est = grads_l(z:z+numel(n.W{2})-1);
            z = z + numel(n.W{2});
            w3_est = grads_l(z:z+numel(n.W{3})-1);
            z = z + numel(n.W{3});
            q1_est = grads_l(z:z+numel(n.q{1})-1);
            z = z + numel(n.q{1});
            q2_est = grads_l(z:z+numel(n.q{2})-1);
            z = z + numel(n.q{2});
            q3_est = grads_l(z:z+numel(n.q{3})-1);
            
            % Check everything
            testCase.assertLessThan(abs(w1_est - [res.w1_11; res.w1_21; res.w1_10; res.w1_12; res.w1_22; res.w1_20]), 10^-6);
            testCase.assertLessThan(abs(w2_est - [res.w2_11; res.w2_21; res.w2_10; res.w2_12; res.w2_22; res.w2_20]), 10^-6);
            testCase.assertLessThan(abs(w3_est - [res.w3_1; res.w3_2; res.w3_0]), 10^-6);
            testCase.assertLessThan(abs(q1_est - [res.q1_1; res.q1_2]), 10^-6);
            testCase.assertLessThan(abs(q2_est - [res.q2_1; res.q2_2]), 10^-6);
            testCase.assertLessThan(abs(q3_est - res.q3), 10^-6);

        end
        
        function testBackwardPass_update_layer(testCase)
            
            n = SplineNN('', 2, 'spline_update', SplineNN.UPDATE_LAYER);
            X = [0.1 -0.2;];
            Y = [0.6 0.1;];
            
            n = n.init(X, Y, struct());
            
            % Set random weights
            n.W = {randn(3, 2)*0.2, randn(3, 2)*0.2};
            
            % Set random spline control points
            n.q{1} = rand(n.N_control_points, 1);
            n.q{2} = rand(n.N_control_points, 1);
            
            % Make forward pass in network
            [pre, post, opt] = n.forwardPass(X);

            % Make backward pass in network
            grads_l = n.backwardPass(X, Y, pre, post, opt);
            
            % Make manual backward pass
            res = testCase.makeManualBackwardPass(n, X, Y, pre, post);
            
            % Extract subvectors from computer gradients' vector
            wh_est = grads_l(1:numel(n.W{1}));
            z = numel(n.W{2}) + 1;
            wo_est = grads_l(z:z+numel(n.W{2})-1);
            z = z + numel(n.W{2});
            qh_est = grads_l(z:z+numel(n.q{1})-1);
            z = z + numel(n.q{1});
            qo_est = grads_l(z:z+numel(n.q{2})-1);
            
            % Check everything
            testCase.assertLessThan(abs(qo_est - (res.qo_1 + res.qo_2)), 10^-6);
            testCase.assertLessThan(abs(wo_est- [res.wo_11; res.wo_21; res.wo_1; res.wo_12; res.wo_22; res.wo_2]), 10^-6);
            testCase.assertLessThan(abs(qh_est - (res.qh_1 + res.qh_2)), 10^-6);
            testCase.assertLessThan(abs(wh_est - [res.wh_11; res.wh_21; res.wh_1; res.wh_12; res.wh_22; res.wh_2]), 10^-6);
            
        end
        
        function testBackwardPass_update_single(testCase)
            
            n = SplineNN('', 2, 'spline_update', SplineNN.UPDATE_SINGLE);
            X = [0.1 -0.2;];
            Y = [0.6 0.1;];
            
            n = n.init(X, Y, struct());
            
            % Set random weights
            n.W = {randn(3, 2)*0.2, randn(3, 2)*0.2};
            
            % Set random spline control points
            n.q{1} = rand(n.N_control_points, 1);
            
            % Make forward pass in network
            [pre, post, opt] = n.forwardPass(X);

            % Make backward pass in network
            grads_l = n.backwardPass(X, Y, pre, post, opt);
            
            % Make manual backward pass
            res = testCase.makeManualBackwardPass(n, X, Y, pre, post);
            
            % Extract subvectors from computer gradients' vector
            wh_est = grads_l(1:numel(n.W{1}));
            z = numel(n.W{2}) + 1;
            wo_est = grads_l(z:z+numel(n.W{2})-1);
            z = z + numel(n.W{2});
            q_est = grads_l(z:z+numel(n.q{1})-1);
            
            % Check everything
            testCase.assertLessThan(abs(q_est - (res.qo_1 + res.qo_2 + res.qh_1 + res.qh_2)), 10^-6);
            testCase.assertLessThan(abs(wo_est- [res.wo_11; res.wo_21; res.wo_1; res.wo_12; res.wo_22; res.wo_2]), 10^-6);
            testCase.assertLessThan(abs(wh_est - [res.wh_11; res.wh_21; res.wh_1; res.wh_12; res.wh_22; res.wh_2]), 10^-6);
            
        end

    end 
    
    methods
        function res = makeManualBackwardPass(obj, n, X, Y, pre, post)

            % Compute errors
            e1 = (post{2}(1) - Y(1));
            e2 = (post{2}(2) - Y(2));
            
            % Prestore all the indexes
            [u1, u1Index] = getSplineIndexes(n, pre{1});
            [u2, u2Index] = getSplineIndexes(n, pre{2});
            
            % Gradients for qo
            qo_1 = 2*e1*n.B'*[u2(1)^3; u2(1)^2; u2(1); 1];
            qo_2 = 2*e2*n.B'*[u2(2)^3; u2(2)^2; u2(2); 1];
            res.qo_1 = [zeros(u2Index(1)-1, 1); qo_1; zeros(n.N_control_points - u2Index(1)-3, 1)];
            res.qo_2 = [zeros(u2Index(2)-1, 1); qo_2; zeros(n.N_control_points - u2Index(2)-3, 1)];
            
            % Precompute all spline derivatives
            [dphi_h] = n.getSplineDerivatives(pre{1}, u1', u1Index', 1);
            dphi_h1 = dphi_h(1);
            dphi_h2 = dphi_h(2);
            dphi_o = n.getSplineDerivatives(pre{2}, u2', u2Index', 2);
            dphi_o1 = dphi_o(1);
            dphi_o2 = dphi_o(2);
            
            % Gradients for wo
            res.wo_11 = 2*e1*dphi_o1*post{1}(1);
            res.wo_12 = 2*e2*dphi_o2*post{1}(1);
            res.wo_21 = 2*e1*dphi_o1*post{1}(2);
            res.wo_22 = 2*e2*dphi_o2*post{1}(2);
            res.wo_1 = 2*e1*dphi_o1;
            res.wo_2 = 2*e2*dphi_o2;
            
            % Gradients for qh
            qh_1 = 2*e1*dphi_o1*n.W{2}(1,1)*n.B'*[u1(1)^3; u1(1)^2; u1(1); 1] + ...
                2*e2*dphi_o2*n.W{2}(1, 2)*n.B'*[u1(1)^3; u1(1)^2; u1(1); 1];
            qh_2 = 2*e1*dphi_o1*n.W{2}(2,1)*n.B'*[u1(2)^3; u1(2)^2; u1(2); 1] + ...
                2*e2*dphi_o2*n.W{2}(2, 2)*n.B'*[u1(2)^3; u1(2)^2; u1(2); 1];
            res.qh_1 = [zeros(u1Index(1)-1, 1); qh_1; zeros(n.N_control_points - u1Index(1)-3, 1)];
            res.qh_2 = [zeros(u1Index(2)-1, 1); qh_2; zeros(n.N_control_points - u1Index(2)-3, 1)];
            
            % Gradients for wh
            res.wh_11 = 2*e1*dphi_o1*n.W{2}(1,1)*dphi_h1*X(1) + ...
                2*e2*dphi_o2*n.W{2}(1,2)*dphi_h1*X(1);
            res.wh_12 = 2*e1*dphi_o1*n.W{2}(2,1)*dphi_h2*X(1) + ...
                2*e2*dphi_o2*n.W{2}(2,2)*dphi_h2*X(1);
            res.wh_21 = 2*e1*dphi_o1*n.W{2}(1,1)*dphi_h1*X(2) + ...
                2*e2*dphi_o2*n.W{2}(1,2)*dphi_h1*X(2);
            res.wh_22 = 2*e1*dphi_o1*n.W{2}(2,1)*dphi_h2*X(2) + ...
                2*e2*dphi_o2*n.W{2}(2,2)*dphi_h2*X(2);
            res.wh_1 = 2*e1*dphi_o1*n.W{2}(1,1)*dphi_h1 + ...
                2*e2*dphi_o2*n.W{2}(1,2)*dphi_h1;
            res.wh_2 = 2*e1*dphi_o1*n.W{2}(2,1)*dphi_h2 + ...
                2*e2*dphi_o2*n.W{2}(2,2)*dphi_h2;
            
            
        end
    end
    
end

