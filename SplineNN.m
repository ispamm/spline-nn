classdef SplineNN < StandardNN
    % SPLINENN - A neural network with flexible activation functions
    
    properties
        B;                  % Base matrix for the spline
        limit;              % Spline limit
        sampling_step;      % Sampling step
        spline_init_fcn;    % Initialization function for the splines
        spline_init_prob;   % Sampling probability of adding noise during initialization
        spline_init_noise;  % Variance of noise during initialization
        control_points;     % Control points for the spline
        q;                  % Spline values
        N_control_points;   % Number of control points for each spline.
        q0;                 % Initial values for the spline
        spline_update;      % Type of update
    end
    
    properties(Constant)
        UPDATE_ALL = 1;     % Update every spline independently
        UPDATE_LAYER = 2;   % Update a single spline per layer
        UPDATE_SINGLE = 3;  % Update a single spline per network
    end
    
    methods
        
        function obj = SplineNN(name, N_hidden, varargin)
            
            % Initialize the network
            obj = obj@StandardNN(name, N_hidden, varargin{:});
            
            p = inputParser();
            p.KeepUnmatched = true;
            p.addParamValue('spline_init_fcn', @tanh);
            p.addParamValue('spline_init_prob', 0.1);
            p.addParamValue('spline_init_noise', 0);
            p.addParamValue('spline_update', SplineNN.UPDATE_ALL);
            p.addParamValue('sampling_step', 0.2);
            p.addParamValue('limit', 2);
            p.addParamValue('spline_type', 'catmulrom');
            p.parse(varargin{:});
            
            obj.spline_init_fcn = p.Results.spline_init_fcn;
            obj.spline_init_prob = p.Results.spline_init_prob;
            obj.spline_init_noise = p.Results.spline_init_noise;
            obj.spline_update = p.Results.spline_update;
            obj.sampling_step = p.Results.sampling_step;
            obj.limit = p.Results.limit;
            
            % Initialize the basis matrix
            if(strcmp(p.Results.spline_type, 'catmulrom'))
                obj.B = 0.5*[-1  3 -3  1; ...
                    2 -5  4 -1; ...
                    -1  0  1  0; ...
                    0  2  0  0];
            elseif(strcmp(p.Results.spline_type, 'bspline'))
                obj.B = (1/6)*[-1  3 -3  1; ...
                    3 -6  3  0; ...
                    -3  0  3  0; ...
                    1  4  1  0];
            else
                error('Spline type not recognized, must be ''catmulrom'' or ''bspline''');
            end
        end
        
        function obj = init(obj, X, Y, options)
            
            % Initialize x-axis control points
            obj.control_points = -obj.limit - obj.sampling_step:obj.sampling_step:obj.limit + obj.sampling_step;
            
            % Require a column vector
            obj.control_points = obj.control_points(:);
            obj.N_control_points = length(obj.control_points);
            
            % Initialize splines activation functions
            obj.q = cell(length(obj.N_hidden) + 1, 1);
            obj.q0 = cell(length(obj.N_hidden) + 1, 1);
            
            % Initialize weights coefficients
            obj = obj.init@StandardNN(X, Y, options);

            af_values = obj.spline_init_fcn(obj.control_points);
            
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                % q{ii}
                for ii = 1:length(obj.N_hidden)
                    obj.q0{ii} = repmat(af_values, 1, obj.N_hidden(ii));
                end
                obj.q0{ii+1} = repmat(af_values, 1, obj.N_outputs);
            elseif(obj.spline_update == SplineNN.UPDATE_LAYER)
                % Just one spline per layer
                for ii = 1:length(obj.N_hidden)+1
                    obj.q0{ii} = af_values;
                end
            else
                % We only have a single spline
                obj.q0 = {af_values};
            end
            
            if(~strcmp(obj.initMethod, 'autoencoder'))
                obj.q = obj.q0;
            else
                obj.q{end} = obj.q0{end};
            end
            
            % Add some noise
            for k = 1:length(obj.q)
               % Extract given percentage of control points
               q_idx = randsample(numel(obj.q{k}), ceil(obj.spline_init_prob*numel(obj.q{k})));
               obj.q{k}(q_idx) = obj.q{k}(q_idx) + randn(length(q_idx), 1)*obj.spline_init_noise;
            end

            obj.q0 = obj.vectorizeCellArray(obj.q0);
            obj.theta = obj.getWeights();
            
        end
        
        function obj = getParametersFromAutoencoder(obj, autoencoder, k)
            obj = obj.getParametersFromAutoencoder@StandardNN(autoencoder, k);
            obj.q{k} = autoencoder.q{1};
        end
        
        function [preActFunc, postActFunc, opt] = forwardPass(obj, X)
            
            N = size(X, 1);
            
            preActFunc = cell(obj.N_layers, 1);
            postActFunc = cell(obj.N_layers, 1);
            
            u = cell(obj.N_layers, 1);
            u_index = cell(obj.N_layers, 1);
            u_vect = cell(obj.N_layers, 1);
            
            % Input-to-hidden pass
            preActFunc{1} = [X ones(N, 1)]*obj.W{1};
            [postActFunc{1}, u{1}, u_index{1}, u_vect{1}] = obj.getSplineValues(preActFunc{1}, 1);
            
            % Hidden-to-hidden pass
            for k = 2:length(obj.N_hidden)
                preActFunc{k} = [postActFunc{k-1} ones(N, 1)]*obj.W{k};
                [postActFunc{k}, u{k}, u_index{k}, u_vect{k}] = obj.getSplineValues(preActFunc{k}, k);
            end
            
            % Hidden-to-output pass
            preActFunc{end} = [postActFunc{end-1} ones(N, 1)]*obj.W{end};
            [postActFunc{end}, u{end}, u_index{end}, u_vect{end}] = obj.getSplineValues(preActFunc{end}, obj.N_layers);
            
            % Save the auxiliary information on the current forward pass
            opt.u = u;
            opt.u_index = u_index;
            opt.u_vect = u_vect;
            
        end
        
        function [grads_error, grads_weights] = backwardPass(obj, X, Y, preActFunc, postActFunc, opt, options)
            % Make a backward pass to compute the derivatives of a squared
            % error function. The first two inputs must be gathered from a
            % call to 'forwardPass'. The second output represents the
            % derivatives of a squared regularizer on the weights.
            
            N = size(X, 1);
            
            % Initialize output structures
            z_q = length(obj.theta);
            z_w = obj.W_numel;
            grads_error = zeros(length(obj.theta), 1);
            grads_q = cell(obj.N_layers, 1);
            
            % Compute the error
            error = Y - postActFunc{end};
            
            % Reshape as a column vector to vectorize everything
            error_vect = error(:);
            
            % Gradients for splines control points of last layer (for each
            % pair input/output)
            u_index = opt.u_index{end};
            u_vect = opt.u_vect{end}';
            
            % Get gradients only for knots
            grads = -2*repmat(error_vect', 4, 1).*(obj.B'*u_vect);
            
            % Reshape gradients
            grads_q{end} = obj.reshapeSplineGradients(obj.N_outputs*N, grads, u_index);
            
            % Compute intermediate value for output layer
            delta = -2*error.*obj.getSplineDerivatives(preActFunc{end}, opt.u{end}, u_index, obj.N_layers);
            
            % Compute gradients for output connections
            grads_error(z_w-numel(obj.W{end})+1:z_w) = [postActFunc{end-1} ones(N, 1)]'*delta;
            z_w = z_w - numel(obj.W{end});
            
            % Gradients for hidden-hidden layers
            for k = obj.N_layers-1:-1:2
                
                % Gradients for splines
                u_index = opt.u_index{k};
                u_vect = opt.u_vect{k}';

                delta = delta*(obj.W{k+1}(1:end-1, :)');
                grads = repmat([delta(:)'], 4, 1).*(obj.B'*u_vect);

                % Update the corresponding span in the gradient matrix
                grads_q{k} = obj.reshapeSplineGradients(obj.N_hidden(k)*N, grads, u_index);
                
                % Compute gradients for output connections
                delta = delta.*obj.getSplineDerivatives(preActFunc{k}, opt.u{k}, u_index, k);
                grads_error(z_w-numel(obj.W{k})+1:z_w) = [postActFunc{k-1} ones(N, 1)]'*delta;
                z_w = z_w - numel(obj.W{k});

            end

            % Gradients for input splines
            u_index = opt.u_index{1};
            u_vect = opt.u_vect{1}';
            
            delta = delta*(obj.W{2}(1:end-1, :)');
            grads = repmat([delta(:)'], 4, 1).*(obj.B'*u_vect);
            
            % Update the corresponding span in the gradient matrix
            grads_q{1} = obj.reshapeSplineGradients(obj.N_hidden(1)*N, grads, u_index);
            
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                % Sum all gradients for each output
                o = squeeze(sum(reshape(grads_q{end}, [obj.N_control_points, N, obj.N_outputs]), 2));
                grads_error(z_q-obj.N_control_points*obj.N_outputs+1:z_q) = o;
                z_q = z_q - obj.N_control_points*obj.N_outputs;
                for(ii = length(obj.N_hidden):-1:1)
                    o = squeeze(sum(reshape(grads_q{ii}, [obj.N_control_points, N, obj.N_hidden(ii)]), 2));
                    grads_error(z_q-obj.N_control_points*obj.N_hidden(ii)+1:z_q) = o(:);
                    z_q = z_q - obj.N_control_points*obj.N_hidden(ii);
                end
            elseif(obj.spline_update == SplineNN.UPDATE_LAYER)
                % Sum all gradients
                for(ii = length(obj.N_hidden)+1:-1:1)
                    grads_error(z_q-obj.N_control_points+1:z_q) = sum(grads_q{ii}, 2);
                    z_q = z_q - obj.N_control_points;
                end
            else
                for(ii = 1:length(obj.N_hidden)+1)
                    grads_error(z_q-obj.N_control_points+1:z_q) = grads_error(z_q-obj.N_control_points+1:z_q) + ...
                        sum(grads_q{ii}, 2);
                end
            end
            
            % Gradients for input connections
            delta = delta.*obj.getSplineDerivatives(preActFunc{1}, opt.u{1}, u_index, 1);
            grads_error(z_w-numel(obj.W{1})+1:z_w) = [X ones(N, 1)]'*delta;
            
            grads_error = grads_error/size(Y, 1);
            
            if(nargout == 2)
                % Gradients for the L2-norm of the weights
                grads_weights = [2*options.lambda*obj.theta(1:obj.W_numel); ...
                    2*options.lambda_q0*(obj.theta(obj.W_numel+1:end) - obj.q0)];
            end
            
        end
        
        function g = reshapeSplineGradients(obj, s, grads, u_index)
                % Update the corresponding span in the gradient matrix
                g = zeros(obj.N_control_points, s);
                lin_indices = sub2ind(size(g), u_index, [1:size(g,2)]');
                lin_indices = [lin_indices, lin_indices + 1, lin_indices + 2, lin_indices + 3]';
                g(lin_indices(:)) = grads(:);
        end
        
        function fval = computeObjectiveFunction(obj, ~, Y, ~, post, w, ~, options)
            fval = sum(sum((Y - post{end}).^2))/size(Y, 1) + options.lambda*sum(w(1:obj.W_numel).^2);
            if(options.lambda_q0 > 0)
                fval = fval + options.lambda_q0*sum((w(obj.W_numel+1:end) - obj.q0).^2);
            end
        end
        
        function [obj, fval, gval] = train(obj, X, Y, options)
            [obj, fval, gval] = obj.train@StandardNN(X, Y, options);
        end
        
        function scores = test(obj, X)
            [~, scores] = obj.forwardPass(X);
            scores = scores{end};
        end
        
        function w = getWeights(obj)
            % Get a vector of parameters
            w = [obj.vectorizeCellArray(obj.W); obj.vectorizeCellArray(obj.q)];
        end
        
        function obj = reshapeWeights(obj, theta)
            % Set the weights from a vector
            obj = obj.reshapeWeights@StandardNN(theta(1:obj.W_numel));
            obj.theta = theta;
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                z = obj.W_numel + 1;
                for ii = 1:length(obj.N_hidden)
                    obj.q{ii} = reshape(theta(z:z+numel(obj.q{ii})-1), obj.N_control_points, obj.N_hidden(ii));
                    z = z + numel(obj.q{ii});
                end
                obj.q{end} = reshape(theta(z:z+numel(obj.q{end})-1), obj.N_control_points, obj.N_outputs);
            elseif(obj.spline_update == SplineNN.UPDATE_LAYER)
                z = obj.W_numel + 1;
                for ii = 1:length(obj.N_hidden)
                    obj.q{ii} = theta(z:z+obj.N_control_points-1);
                    z = z + obj.N_control_points;
                end
                obj.q{end} = theta(z:z+obj.N_control_points-1);
            else
                obj.q{1} = theta(obj.W_numel+1:obj.W_numel+1+obj.N_control_points-1);
            end
        end
        
        function [v, u, uIndex, u_vect] = getSplineValues(obj, preActivation, layer)
            
            % Get indexes of the splines
            [u, uIndex] = obj.getSplineIndexes(preActivation);
            u = u(:);
            uIndex = uIndex(:);
            
            % Construct the basis vector
            u_vect = [u.^3, u.^2, u, ones(length(u), 1)];

            % Get corresponding spline values (for interpolation).
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                out_idx = repmat(1:size(preActivation, 2), size(preActivation, 1), 1);
                lin_indices = sub2ind(size(obj.q{layer}), uIndex, out_idx(:));
                q_mat = obj.q{layer}([lin_indices, lin_indices + 1, lin_indices + 2, lin_indices + 3]);
            elseif(obj.spline_update == SplineNN.UPDATE_LAYER)
                q_mat = obj.q{layer}([uIndex, uIndex + 1, uIndex + 2, uIndex + 3]);
            else
                % Ignore layer parameter
                q_mat = obj.q{1}([uIndex, uIndex + 1, uIndex + 2, uIndex + 3]);
            end
            
            % Compute values
            if(numel(u) > 1)
                v = dot(u_vect*obj.B, q_mat, 2);
                % Reshape the values
                v = reshape(v, size(preActivation, 1), size(preActivation, 2));
            else
                v = u_vect*obj.B*q_mat;
            end

        end
        
        function [u, uIndex] = getSplineIndexes(obj, preActivation)
            Su = preActivation./obj.sampling_step + (obj.N_control_points-1)/2;
            uIndex =  floor(Su);
            u = Su-uIndex;
            % Bound checking
            uIndex(uIndex<1) = 1;
            % The index must fit
            uIndex(uIndex>(obj.N_control_points-3)) = obj.N_control_points - 3;
        end
        
        function [dx, ddx] = getSplineDerivatives(obj, preActivation, u, uIndex, layer)
            du = [3*u.^2, 2*u, ones(length(u), 1), zeros(length(u), 1)];
            
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                out_idx = repelem(0:obj.N_control_points:obj.N_control_points*size(preActivation, 2)-1, size(preActivation, 1))';
                lin_indices = uIndex + out_idx;
                q_mat = obj.q{layer}([lin_indices, lin_indices + 1, lin_indices + 2, lin_indices + 3]);
            elseif(obj.spline_update == SplineNN.UPDATE_LAYER)
                q_mat = obj.q{layer}([uIndex, uIndex + 1, uIndex + 2, uIndex + 3]);
            else
                % Ignore layer parameter
                q_mat = obj.q{1}([uIndex, uIndex + 1, uIndex + 2, uIndex + 3]);
            end
            
            if(numel(u) > 1)
                dx = dot((du*obj.B), q_mat, 2)/obj.sampling_step;
                dx = reshape(dx, size(preActivation, 1), size(preActivation, 2));
            else
                dx = du*obj.B*q_mat/obj.sampling_step;
            end
            
            if(nargout == 2)
                ddu = [6*u, 2*ones(length(u), 1), zeros(length(u), 2)];
                ddx = dot((ddu*obj.B), q_mat, 2)/(obj.sampling_step^2);
                ddx = reshape(ddx, size(preActivation, 1), size(preActivation, 2));
            end
        end
        
        function plot_spline(obj, spline_idx, resolution, color, line_width, marker_number, marker_type, marker_size)
            % Plot a spline function on the currently active figure
            %   Parameters:
            %
            %   - limit (required): limit for plotting.
            %   - resolution (optional): resolution of the x-axis (default
            %   to 500).
            %   - color (optional): color of the line. Can be a string
            %   specification, or a 3-valued vector specifying a RGB color.
            %   Default to 'r'.
            %   - line_width: width of the line, default to 1.
            
            if(nargin < 8)
                marker_size = 6;
            end
            
            if(nargin < 7)
                marker_type = '';
            end
            
            if(nargin < 6)
                marker_number = 0;
            end
            
            if(nargin < 5)
                line_width = 1;
            end
            
            if(nargin < 4)
                color = 'r';
            end
            
            if(nargin < 3)
                resolution = 500;
            end
            
            if(obj.spline_update == SplineNN.UPDATE_ALL)
                found = false;
                for ii = 1:length(obj.N_hidden)
                   if(spline_idx <= obj.N_hidden(ii))
                       q_i = obj.q{ii}(:, spline_idx);
                       found = true;
                   else
                       spline_idx = spline_idx - obj.N_hidden(ii);
                   end
                end
                if(~found)
                    q_i = obj.q{end}(:, spline_idx);
                end
            else
                q_i = obj.q{spline_idx};
            end
            
            fakeNet = obj;
            fakeNet.q{1} = q_i;
            
            x1 = -obj.limit:(2*obj.limit/resolution):obj.limit;
            yy2 = fakeNet.getSplineValues(x1', 1);
            
            if(ischar(color))
                if(marker_number == 0)
                    plot(x1, yy2, color, 'LineWidth', line_width, 'MarkerSize', marker_size);
                else
                    line_fewer_markers(x1, yy2, marker_number, color, 'LineWidth', line_width, 'MarkerSize', marker_size);
                end
            else
                if(marker_number == 0)
                    plot(x1, yy2, 'LineWidth', line_width, 'Color', color, 'MarkerSize', marker_size);
                else
                    line_fewer_markers(x1, yy2, marker_number, marker_type, 'LineWidth', line_width, 'Color', color, 'MarkerSize', marker_size);
                end
            end
            
        end
        
        function plot_multiple_splines(obj, spline_idx, resolution)
            % Plot multiple spline functions (utility function for calling
            % multiple times 'plot_spline').
            if(nargin < 3)
                resolution = 500;
            end
            colors = linspecer(length(spline_idx));
            figure();
            hold on;
            for i = 1:length(spline_idx)
                obj.plot_spline(spline_idx(i), resolution, colors(i,:));
            end
            
        end
        
    end
    
end

