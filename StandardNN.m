classdef StandardNN
    % STANDARDNN - A standard neural network with a variable number of
    % hidden layers
    
    properties
        name;           % Name
        N_inputs;       % Number of inputs
        N_hidden;       % Size of hidden layer (scalar or vector)
        N_outputs;      % Number of outputs
        W;              % Cell array of parameters
        N_params;       % Number of parameters
        N_params_cum;   % Cumulative number of parameters up to layer i
        N_layers;       % Number of layers (hidden + output)
        actFunc;        % Activation function
        dActFunc;       % Derivative of the activation function
        optimizer;      % Optimizer for training
        initMethod;     % Initialization method (string)
        noiseEncoder;   % Noise to be added during autoencoder initialization
        theta;          % Parameters of the network as a single vector
        W_numel;        % Number of parameters (repeated)
    end
    
    methods
        
        function obj = StandardNN(name, N_hidden, varargin)
            obj.name = name;
            obj.N_hidden = N_hidden;
            p = inputParser();
            p.KeepUnmatched = true;
            p.addParamValue('optimizer', CGOptimizer());
            p.addParamValue('actFunc', 'tanh');
            p.addParamValue('initMethod', 'glorot');
            p.addParamValue('noiseEncoder', 0.25);
            p.parse(varargin{:});
            obj.optimizer = p.Results.optimizer;
            obj.initMethod = p.Results.initMethod;
            obj.noiseEncoder = p.Results.noiseEncoder;
            if(strcmp(p.Results.actFunc, 'tanh'))
                obj.actFunc = @(s) tanh(s);
                obj.dActFunc = @(s) 1-tanh(s).^2;
            else
                error('Activation function not recognized');
            end
        end
        
        function obj = init(obj, X, Y, options)

            % Save auxiliary variables
            obj.N_inputs = size(X, 2);
            obj.N_outputs = size(Y, 2);
            
            obj.W = cell(length(obj.N_hidden) + 1, 1);
            obj.N_params = zeros(length(obj.N_hidden) + 1, 1);
            
            if(strcmp(obj.initMethod, 'glorot'))

                % Initialize the weights of the network, from the uniform 
                % distribution according to Glorot and Bengio (2010).
                
                % Input to hidden weights
                r = sqrt(6/(obj.N_inputs+obj.N_hidden(1)));
                obj.W{1} = rand(obj.N_inputs + 1, obj.N_hidden(1))*2*r - r;
                obj.W{1}(end, :) = 0; % Set to zero the biases

                % Hidden-to-hidden weights
                for k = 2:length(obj.N_hidden)
                    r = sqrt(6/(obj.N_hidden(k-1)+obj.N_hidden(k)));
                    obj.W{k} = rand(obj.N_hidden(k-1) + 1, obj.N_hidden(k))*2*r - r;
                    obj.W{k}(end, :) = 0; % Set to zero the biases
                end

            elseif(strcmp(obj.initMethod, 'autoencoder'))
                
                input = X;
                for k = 1:length(obj.N_hidden)
                   
                    % Add noise to current input
                    output = X;
                    output(rand(size(output, 1), size(output, 2)) < obj.noiseEncoder) = 0;
                    
                    % Initialize and train the autoencoder
                    net = obj;
                    net.initMethod = 'glorot';
                    net.N_hidden = obj.N_hidden(k);
                    net = net.init(input, output, options.autoencoder);
                    [net, fval, gval] = net.train(input, output, options.autoencoder);
                    
                    % Get the parameters
                    obj = obj.getParametersFromAutoencoder(net, k);
 
                    % Set new input
                    [~, input] = net.forwardPass(input);
                    input = input{1};
                    
                end
                
            else
                error('Initialization method not recognized');
            end
            
            % Hidden to output weights
            r = sqrt(6/(obj.N_hidden(end)+obj.N_outputs));
            obj.W{end} = rand(obj.N_hidden(end) + 1, obj.N_outputs)*2*r - r;
            obj.W{end}(end, :) = 0; % Set to zero the biases
            
            % Count parameters
            obj.N_layers = length(obj.W);
            obj.theta = obj.getWeights();
            obj.W_numel = numel(obj.theta);
            for k = 1:length(obj.W)
                obj.N_params(k) = numel(obj.W{k});
            end
            obj.N_params_cum = cumsum(obj.N_params);
                
        end
        
        function obj = getParametersFromAutoencoder(obj, autoencoder, k)
            obj.W{k} = autoencoder.W{1};
        end
        
        function [preActFunc, postActFunc, opt] = forwardPass(obj, X)
            % Make a forward pass over the network, returns:
            %   preActFun: cell array of pre-activations for the layers.
            %   postActFunc: cell array of activations for the layers.
            
            N = size(X, 1);
            
            preActFunc = cell(obj.N_layers, 1);
            postActFunc = cell(obj.N_layers, 1);
            
            % Input-to-hidden pass
            preActFunc{1} = [X ones(N, 1)]*obj.W{1};
            postActFunc{1} = obj.actFunc(preActFunc{1});
            
            % Hidden-to-hidden pass
            for k = 2:length(obj.N_hidden)
                preActFunc{k} = [postActFunc{k-1} ones(N, 1)]*obj.W{k};
                postActFunc{k} = obj.actFunc(preActFunc{k});
            end
            
            % Hidden-to-output pass
            preActFunc{end} = [postActFunc{end-1} ones(N, 1)]*obj.W{end};
            postActFunc{end} = obj.actFunc(preActFunc{end});
            
            opt = struct();
            
        end
        
        function [grads_error, grads_weights] = backwardPass(obj, X, Y, preActFunc, postActFunc, ~, options)
            % Make a backward pass to compute the derivatives of a squared
            % error function. The first two inputs must be gathered from a
            % call to 'forwardPass'. The second output represents the
            % derivatives of a squared regularizer on the weights.
            
            N = size(X, 1);
            
            grads_error = zeros(obj.W_numel, 1);
            
            % Gradients for output layer
            error = Y - postActFunc{end};
            delta = -2*error.*obj.dActFunc(preActFunc{end});
            grads_error(obj.N_params_cum(end-1)+1:obj.N_params_cum(end)) = [postActFunc{end-1} ones(N, 1)]'*delta;
            
            % Gradients for hidden-hidden layers
            for k = obj.N_layers-1:-1:2
                % Gradient for input-hidden layer
                delta = delta*(obj.W{k+1}(1:end-1, :)').*obj.dActFunc(preActFunc{k});
                grads_error(obj.N_params_cum(k-1)+1:obj.N_params_cum(k)) = [postActFunc{k-1} ones(N, 1)]'*delta;
            end
            
            % Gradient for input-hidden layer
            delta = delta*(obj.W{2}(1:end-1, :)').*obj.dActFunc(preActFunc{1});
            grads_error(1:obj.N_params_cum(1)) = [X ones(N, 1)]'*delta;
            
            grads_error = grads_error./size(Y, 1);
            if(nargout == 2)
                % Gradients for the L2-norm of the weights
                grads_weights = 2*options.lambda*obj.theta;
            end
            
        end
        
        function fval = computeObjectiveFunction(~, ~, Y, ~, post, w, ~, options)
            % Compute the objective function
            fval = sum(sum((Y - post{end}).^2))/size(Y, 1) + options.lambda*sum(w.^2);
        end
        
        function [obj, fval, gval] = train(obj, X, Y, options)
            obj = obj.init(X, Y, options);
            [obj, fval, gval] = obj.optimizer.optimize(obj, X, Y, options);
        end
           
        
        function scores = test(obj, X)
            [~, scores] = obj.forwardPass(X);
            scores = scores{end};
        end
        
        function w = getWeights(obj)
            % Get a vector of parameters
            w = obj.vectorizeCellArray(obj.W);
        end
        
        function w = vectorizeCellArray(obj, c)
            % Get a vector of parameters
            w = cellfun(@(X) X(:), c, 'UniformOutput', false);
            w = vertcat(w{:});
        end
        
        function obj = reshapeWeights(obj, theta)
            % Set the weights from a vector
            obj.theta = theta;
            obj.W{1} = reshape(theta(1:obj.N_params(1)), obj.N_inputs+1, obj.N_hidden(1));
            for k = 2:obj.N_layers-1
               obj.W{k} = reshape(theta(obj.N_params_cum(k-1)+1:obj.N_params_cum(k-1)+obj.N_params(k)), ...
                   obj.N_hidden(k-1)+1, obj.N_hidden(k)); 
            end
            obj.W{end} = reshape(theta(obj.N_params_cum(end-1)+1:end), obj.N_hidden(end) + 1, obj.N_outputs);
        end
        
    end
    
end

