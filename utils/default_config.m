function c = default_config()
c = struct('lambda', 10^-5, 'lambda_q0', 10^-5, 'epochs', 1000, 'batch_size', 30, ...
    'eta', 0.001, 'beta1', 0.9, 'beta2', 0.999, 'eps_adam', 10^-8);
end