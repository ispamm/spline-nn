function NRMSE = nrmse(correctOutput, estimatedOutput)

nEstimatePoints = size(correctOutput, 1) ; 

correctVariance = var(correctOutput) ; 
meanerror = sum((estimatedOutput - correctOutput).^2)/nEstimatePoints ; 

NRMSE = (sqrt(meanerror./correctVariance)) ; 
