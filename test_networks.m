
% -------------------------------------------------------------------------
% --- CONFIGURATION -------------------------------------------------------
% -------------------------------------------------------------------------

% Reset environment
clear all; close all; clc;

% Set PRNG seed
rng(1);

% Add folders
addpath(genpath('./utils/'), 'datasets');

% Global parameters
N_runs = 1; % Number of runs

% We test a standard NN and a spline NN with the same number of parameters.
% You can add/modify additional algorithms as elements of the cell array.
algorithms = {...
    StandardNN('NN', 5), ...
    SplineNN('S-NN', 5), ...
};

% General parameters (default configuration)
options = default_config();

% If you want to perform a grid-search on the parameters, uncomment these
% instructions. Parameters to test are defined inside a dictionary.
params = containers.Map;
% params('NN') = containers.Map({'lambda'}, {10.^(-8:-1)});
% params('S-NN') = containers.Map({'lambda', 'lambda_q0'}, {10.^(-8:-1), 10.^(-8:-1)});

% Load dataset
load MATLAB_chemical
% load statlib_calhousing

% Input normalization
X = mapminmax(X', -1, 1); X = X';

% Select appropriate error function and output normalization
if(strcmp(task, 'R')) % Regression
    % Define the error function (nrmse)
    options.err_fcn = @nrmse;
    % Uncomment for standard mean-squared error
    %options.err_fcn = @(Y_true, Y_pred) sum(sum((Y_true - Y_pred).^2))/size(Y_true, 1);
    % Output normalization
    Y = mapminmax(Y', -0.5, 0.5); 
    Y = Y';
    % Compute a baseline error
    baseline_error = options.err_fcn(Y, mean(Y));
elseif(strcmp(task, 'BC')) % Binary classification (to test)
    options.err_fcn = @(Y_true, Y_pred) mean(sign(Y_true') ~= sign(Y_pred'));
elseif(strcmp(task, 'MC')) % Multiclass classification (to test)
    options.err_fcn = @(Y_true, Y_pred) mean(vec2ind(Y_true') ~= vec2ind(Y_pred'));
else
    error('Dataset task not recognized, must be R, BC or MC');
end

% Initialize output structures
nets = cell(length(algorithms), N_runs);
errors_training = zeros(length(algorithms), N_runs);
errors_test = zeros(length(algorithms), N_runs);
times = zeros(length(algorithms), N_runs);
fval = zeros(length(algorithms), options.epochs);
gval = zeros(length(algorithms), options.epochs);

for r = 1:N_runs

    fprintf('Run %i/%i...\n', r, N_runs);
    
    % Split the dataset
    holdout = cvpartition(size(X, 1), 'Holdout', 0.33);
    X_trn = X(holdout.training(), :);
    X_tst = X(holdout.test(), :);
    Y_trn = Y(holdout.training(), :);
    Y_tst = Y(holdout.test(), :);
    
    % Compute a single 3-fold partition
    cv = cvpartition(size(Y_trn, 1), 'K', 3);
    
    for a = 1:length(algorithms)
       
        % Print on screen the name of the algorithm
        fprintf('\tTraining %s...\n', algorithms{a}.name);
        
        % Set the original options
        options_curr = options;
        
        if(isKey(params, algorithms{a}.name))
        
            % The user requested a grid-search
            
            % Get the parameters
            p = params(algorithms{a}.name);
            
            % Extract parameters to test
            names = p.keys();
            values = p.values();
        
            bestParams = zeros(length(names), 1);
            bestPerf = Inf;

            combinations = combvec(values{:});
            N_combinations = size(combinations, 2);

            if(N_combinations > 1)
            
                % -------------------------------------------------------------
                % This ensures that textprogress bar does not throw any error
                global strCR;
                strCR = '';
                % -------------------------------------------------------------

                fprintf('\t\t ParameterSweep: Evaluating %i settings --> ', N_combinations);
                textprogressbar(' ');
                textprogressbar(0.0);

                % Compute when we should update the screen for the
                % textprogressbar
                tpb_update = ceil(N_combinations/100);

                for zz=1:N_combinations

                    % Update the textprogressbar on screen
                    if(zz > 1 && mod(zz, tpb_update) == 0)
                        textprogressbar(zz*100/N_combinations);
                    end

                    % Set the current configuration
                    paramsToTest = combinations(:,zz);
                    for i=1:length(names)
                        options_curr.(names{i}) = paramsToTest(i);
                    end

                    % Perform the 3-fold evaluation
                    currPerf = 0;
                    for k = 1:3
                        net = algorithms{a}.train(X_trn(cv.training(k), :), Y_trn(cv.training(k), :), options_curr);
                        currPerf = currPerf + options_curr.err_fcn(Y_trn(cv.test(k), :), net.test(X_trn(cv.test(k), :)));
                    end
                    currPerf = currPerf/3;

                    % Check if we have a new best
                    if(currPerf < bestPerf)
                        bestPerf = currPerf;
                        bestParams = combinations(:, zz);
                    end

                end

                textprogressbar(100);
                textprogressbar('Done');
                
                bestParamsCell = num2cell(bestParams);
                fprintf('\t\t Validated parameters: [ ');
                for i=1:length(bestParamsCell)
                    fprintf('%s = %f ', names{i}, bestParamsCell{i});
                end
                fprintf('], with performance: %.4f\n', bestPerf);

            else % We only have a single combination of parameters
                bestParams = combinations(:, 1);
                bestParamsCell = num2cell(bestParams);
                fprintf('\t\t Only one combination, grid search avoided, parameters: [ ');
                for i=1:length(bestParamsCell)
                    fprintf('%s = %f ', names{i}, bestParamsCell{i});
                end
                fprintf(']\n');
            end

            for i=1:length(names)
                options_curr.(names{i}) = bestParams(i);
            end
            
        end
            
        % Make the final training
        tic;
        [nets{a, r}, fval_curr, gval_curr] = algorithms{a}.train(X_trn, Y_trn, options_curr);
        times(a, r) = toc;
        fval(a, :) = fval(a, :) + fval_curr';
        gval(a, :) = gval(a, :) + gval_curr';
        
        % Test network on training data
        rec_error_ae = options.err_fcn(Y_trn, nets{a, r}.test(X_trn));
        fprintf('\t\tFinal mean-squared error (training): %.5f.\n', rec_error_ae);
        errors_training(a, r) = errors_training(a, r) + rec_error_ae;
        
        % Test network on test data
        rec_error_ae = options.err_fcn(Y_tst, nets{a, r}.test(X_tst));
        fprintf('\t\tFinal mean-squared error (test): %.5f.\n', rec_error_ae);
        errors_test(a, r) = errors_test(a, r) + rec_error_ae;
        
    end
    
end

fval = fval./N_runs;
gval = gval./N_runs;
    
% Compute number of free parameters
free_params = zeros(length(algorithms), 1);
for a = 1:length(algorithms)
    ae_init = algorithms{a};
    ae_init.initMethod = 'glorot';
    ae_init = algorithms{a}.init(X, Y, options);
    free_params(a) = length(ae_init.getWeights());
end

% Print the results
fprintf('\n\n');
% Plot baseline
fprintf('Baseline error on this dataset is %.2f.\n', baseline_error);
names = cellfun(@(a) a.name, algorithms, 'UniformOutput', false);
disptable([mean(errors_training, 2), std(errors_training, 0, 2), ...
    mean(errors_test, 2), std(errors_test, 0, 2), mean(times, 2), std(times, 0, 2), free_params], ...
    {'Tr. Error', 'Tr. Error (tr., std)', 'Tst. Error', 'Tst. Error (std)', 'Time', 'Time (std)', 'Parameters'}, names);

% Plot the results
figure();
title('Objective value');
hold on;
for a = 1:length(algorithms)
    semilogy(fval(a, :));
end
legend(names, 'Location', 'NorthEast');
set(gca, 'yscale', 'log');

% Plot the results
figure();
hold on;
for a = 1:length(algorithms)
    semilogy(gval(a, :));
end
legend(names, 'Location', 'NorthEast');
set(gca, 'yscale', 'log');